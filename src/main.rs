use std::str::FromStr;

#[derive(Debug, PartialEq)]
struct Robot {
    pos_x: i32,
    pos_y: i32,
    orientation: Orientation,
    instructions: Vec<Instructions>,
}

#[derive(Debug, PartialEq, Copy, Clone)]
enum Orientation {
    North,
    East,
    South,
    West,
}

#[derive(Debug, PartialEq, Copy, Clone)]
enum Instructions {
    F,
    L,
    R,
}

impl std::str::FromStr for Orientation {
    type Err = String;

    fn from_str(orien_str: &str) -> Result<Self, Self::Err> {
        match orien_str {
            "N" => Ok(Orientation::North),
            "E" => Ok(Orientation::East),
            "S" => Ok(Orientation::South),
            "W" => Ok(Orientation::West),
            _ => Err(format!("Tourner dans le vide vide")),
        }
    }
}

fn from_char(instru_char: char) -> Result<Instructions, String> {
    match instru_char {
        'F' => Ok(Instructions::F),
        'L' => Ok(Instructions::L),
        'R' => Ok(Instructions::R),
        _ => Err(format!("TAISEZ-VOUS VOus vous")),
    }
}

impl Orientation {
    fn rotate_left(self) -> Orientation {
        match self {
            Orientation::East => Orientation::North,
            Orientation::North => Orientation::West,
            Orientation::South => Orientation::East,
            Orientation::West => Orientation::South,
        }
    }

    fn rotate_right(self) -> Orientation {
        match self {
            Orientation::East => Orientation::South,
            Orientation::North => Orientation::East,
            Orientation::South => Orientation::West,
            Orientation::West => Orientation::North,
        }
    }
}

impl Robot {
    fn forward(bot: &mut Self) {
        match bot.orientation {
            Orientation::East => bot.pos_x += 1,
            Orientation::North => bot.pos_y += 1,
            Orientation::South => bot.pos_y -= 1,
            Orientation::West => bot.pos_x -= 1,
        }
    }

    fn retreat(bot: &mut Self) {
        match bot.orientation {
            Orientation::East => bot.pos_x -= 1,
            Orientation::North => bot.pos_y -= 1,
            Orientation::South => bot.pos_y += 1,
            Orientation::West => bot.pos_x += 1,
        }
    }
}

fn collision(world: &ZaWarudo, robot: &Robot) -> bool {
    let mut boule = false;
    let bot_in_world = world.terminateurs.iter();
    for bot in bot_in_world {
        if bot.pos_x == robot.pos_x && bot.pos_y == robot.pos_y {
            println!("Collision en ({},{})", robot.pos_x, robot.pos_y);
            boule = true;
            break;
        } 
    }
    return boule;
}



fn execute_instr(world: &ZaWarudo, robot: &mut Robot) -> Robot {
    
    match robot.instructions.pop().unwrap() {
        Instructions::F => {
            Robot::forward(robot);
            if collision(world, robot) {
                Robot::retreat(robot);
            }
        },
        Instructions::R => {
            Orientation::rotate_right(robot.orientation);
        },
        Instructions::L => {
            Orientation::rotate_left(robot.orientation);
        },
    }
    return robot;
}


// Dédicace à Quentin et Bibi (Brahima) qui ne savent pas qu'ils
// nous ont aidés concernant la création du monde.

// En cours
#[derive(Debug)]
struct ZaWarudo {
    x_max: i32,
    y_max: i32,
    terminateurs: Vec<Robot>,
}

fn za_warudo(x: i32, y: i32, decepticons: Vec<Robot>) -> Result<(), String> {
    for i in &decepticons {
        if i.pos_x > x || i.pos_y > y || i.pos_x < 0 || i.pos_y < 0 {
            return Err(format!("Les robots ne sont pas tous dans le monde !"));
        }
    }
    let mut monde = ZaWarudo {
        x_max: x,
        y_max: y,
        terminateurs: decepticons,
    };

    println!("Le Big Bang : {:?}", &monde);

    let botinworld = monde.terminateurs.iter_mut();
    for i in botinworld {
        i = execute_instr(&monde, i);
    }

    println!("La Fin du Monde : {:?}", &monde);
    Ok(())
}

fn main() -> std::io::Result<()> {
    // Mini code du prof pour récupérer les arguments de la ligne de commande.
    // lancer avec `cargo run src/two_robots.txt` par exemple
    // Pensez à notifier Claire et Thomas de ce changement ça peut leur plaire ;)
    // Axel: Get command line arguments
    let args: Vec<String> = std::env::args().collect();
    // If not enough argument passed it will crash badlyyyy :)
    let contents = std::fs::read_to_string(&args[1])?;

    let mut lines = contents.lines();
    // Parsing du header du monde x y
    let mut infos = lines.next().expect("Na header").split_whitespace();

    let map_x = infos.next().expect("Noh x_max").parse::<i32>().unwrap();
    let map_y = infos.next().expect("Nay y_max").parse::<i32>().unwrap();
    let mut lairobeau: Vec<Robot> = Vec::new();

    // On itére deux ligne par deux lignes
    // Robots x y o
    // instructions
    while let (Some(robot), Some(instr)) = (lines.next(), lines.next()) {
        let mut robot = robot.split_whitespace();

        let pos_x = robot.next().expect("Noh robot.x").parse::<i32>().unwrap();
        let pos_y = robot.next().expect("Noh robot.y").parse::<i32>().unwrap();
        let orientation = robot.next().expect("NAAAH orientation");
        let orientation = Orientation::from_str(orientation).unwrap();

        let mut bot = Robot {
            pos_x,
            pos_y,
            orientation,
            instructions: Vec::new(),
        };

        for instr in instr.chars() {
            bot.instructions.push(from_char(instr).unwrap());
        }
        lairobeau.push(bot);
    }
    println!("THis is fine");

    za_warudo(map_x, map_y, lairobeau).expect("pas bientot fini?");
    
    Ok(())
}
    

